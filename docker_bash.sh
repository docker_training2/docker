#!/bin/bash
source .env
# checklist version
docker --version
# check docker container running
docker ps -a
# prune docker service all
docker system prune -a
echo "Docker system is prune success"
docker stop $NGINX_CONTAINER_NAME
docker rm $NGINX_CONTAINER_NAME

# pull image from docker hub
docker pull $HELLO_WORLD
echo "Docker pull hello-world images is success"
# running image if don't have port
docker run $HELLO_WORLD
echo "Hello-world is running on the system."
 
# docker pull
docker pull $NGINX
echo "Docker pull nginx is success"
# docker run with port
docker run -d -p $PORT:80 --name $NGINX_CONTAINER_NAME $NGINX
echo "Nginx is running now"
# docker execution to docker container
docker exec -it $NGINX_CONTAINER_NAME bash
echo "Execution docker_nginx success"
exit 0
