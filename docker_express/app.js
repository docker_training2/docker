var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const sequelize = require('./db');
const User = require('./models/user.model');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

(async()=>{
    try {
        await sequelize.authenticate();
        console.log('Database is connected')
        await sequelize.sync({alter: true})
        await User.sync({})
    } catch (error) {
        console.log(error)
    }
})();

app.use('/', indexRouter);
app.use('/users', usersRouter);

module.exports = app;
