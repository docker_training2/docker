source .env

docker stop $SERVICE_NAME && docker rm $SERVICE_NAME
docker build --no-cache -t $SERVICE_NAME:$SERVICE_VERSION .
docker run -d -p $SERVICE_PORT:3000 --name $SERVICE_NAME $SERVICE_NAME:$SERVICE_VERSION
exit 0