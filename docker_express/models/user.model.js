const { DataTypes } = require("sequelize");
const sequelize = require("../db");

const User = sequelize.define('User',{
    id: {
        type : DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    Username: {
        type: DataTypes.STRING(30),
        defaultValue : "",
        allowNull: false
    },
    Password: {
        type: DataTypes.STRING(10),
        defaultValue : "",
        allowNull: false
    }
})

module.exports = User